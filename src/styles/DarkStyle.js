import { createGlobalStyle } from "styled-components";

export const DarkStyle = createGlobalStyle`
  .background {
    filter: brightness(40%);
  }
`