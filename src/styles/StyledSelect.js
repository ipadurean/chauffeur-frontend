import styled from 'styled-components';

export const Select1 = styled.select`
  height: 27px;
  margin: 2px;
  font-size: calc(10px + 0.5vw);
  color: rgb(78, 77, 77);
  width: calc(50px + 5vw);
  outline: none;
`
