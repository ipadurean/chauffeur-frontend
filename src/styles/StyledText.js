import styled from 'styled-components';

export const Title = styled.div`
  font-size: calc(16px + 0.5vw);
  color: #677385;
  margin: 10px;
`

export const Title1 = styled.div`
  font-size: calc(12px + 0.5vw);
  color: #c1c9c7;
  margin: 7px;
`

export const Title2 = styled.div`
  font-size: calc(10px + 0.5vw);
  color: #5c6664;
  margin: 5px;
`

export const Title3 = styled.div`
  font-size: calc(8px + 0.5vw);
  color: #39403e;
  margin: 3px;
`

export const Text = styled.div`
  font-size: calc(8px + 0.5vw);
  color: gray;
`

export const Text1 = styled.div`
  font-size: calc(7px + 0.5vw);
  background-color: #e9f0ea;
  margin: 5px;
  padding: 3px;
  border-radius: 10px;
`

export const Text2 = styled.span`
  font-size: calc(6px + 0.5vw);
  color: #567bb8;
`

export const Text3 = styled.div`
  font-size: calc(5px + 0.5vw);
  color: gray;
`

export const Text4 = styled.div`
  font-family: 'Courier New', Courier, monospace;
  font-size: calc(7px + 0.5vw);
  font-weight: 500;
`